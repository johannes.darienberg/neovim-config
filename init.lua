-- My init.lua

vim.cmd[[
set guicursor=
set termguicolors
set completeopt=menuone,noinsert,noselect
set updatetime=100
set timeoutlen=300
set noshowmode
set list
set listchars=eol:↴


" Local to window
set number relativenumber
set nowrap

" Local to buffer
set tabstop=4
set shiftwidth=4
set noswapfile
set expandtab

let mapleader = " "
]]

local packer = require('packer')

packer.init{}

packer.startup(function(use)

	-- use {
        -- "wbthomason/packer.nvim",

	    -- opt = true
    -- }

	use {
		"rebelot/kanagawa.nvim",

		config = function()

		    require('kanagawa').setup({
    		    undercurl = true,           -- enable undercurls
    		    commentStyle = "italic",
    		    functionStyle = "NONE",
    		    keywordStyle = "italic",
    		    statementStyle = "italic",
    		    typeStyle = "italic",
    		    variablebuiltinStyle = "italic",
    		    specialReturn = true,       -- special highlight for the return keyword
    		    specialException = true,    -- special highlight for exception handling keywords
    		    transparent = false,        -- do not set background color
    		    colors = {},
    	   		overrides = {},
    		})

            vim.cmd [[ colorscheme kanagawa ]]

		end
	}

	use {
		'akinsho/nvim-bufferline.lua',

		config = function()

            require('bufferline').setup {
                options = {
                    numbers = "none",
                    separator_style = "slant",
                    sort_by = "directory",
                    diagnostics = "nvim_lsp",
                    show_buffer_close_icons = false,
                    show_close_icon = false,
                    tab_size = 20,
                    offsets = {},
                },

                highlights = {
                    tab = {
                        guibg = {
                            attribute = "bg",
                            highlight = "TabLine"
                        }
                    },

                    buffer_selected = {
                        gui = "none"
                    }

                },

            }

            -- mappings for changing buffer
            vim.api.nvim_set_keymap('n', '<C-l>',  [[:BufferLineCycleNext<cr>]], { noremap = true, silent = true })
            vim.api.nvim_set_keymap('n', '<C-h>',  [[:BufferLineCyclePrev<cr>]], { noremap = true, silent = true })
		end

	}

    use {
        'nvim-lualine/lualine.nvim',

        config = function()
            require('lualine').setup {
                options = {
                    icons_enabled = false,
                    theme = 'auto',
                    component_separators = { left = '', right = ''},
                    section_separators = { left = '', right = ''},
                    disabled_filetypes = {},
                    always_divide_middle = true,
                },
                sections = {
                    lualine_a = {'mode'},
                    lualine_b = {'branch', 'diff', 'diagnostics'},
                    lualine_c = {'filename'},
                    lualine_x = {'encoding', 'fileformat', 'filetype'},
                    lualine_y = {'progress'},
                    lualine_z = {'location'}
                },
                inactive_sections = {
                    lualine_a = {},
                    lualine_b = {},
                    lualine_c = {'filename'},
                    lualine_x = {'location'},
                    lualine_y = {},
                    lualine_z = {}
                },
                tabline = {},
                extensions = {}
            }
        end
    }

    use {
        'lukas-reineke/indent-blankline.nvim',
        config = function()

        end
    }

    use {
        'nvim-telescope/telescope.nvim',
        requires = {
            {'nvim-lua/plenary.nvim'}
        },

        config = function()
            local actions = require("telescope.actions")

            require("telescope").setup {
                defaults = {
                    mappings = {
                        i = {
                            ["<esc>"] = actions.close
                        },
                    },
                }
            }

            vim.api.nvim_set_keymap('n', '<C-p>',  [[:Telescope find_files<cr>]], { noremap = true, silent = true })

        
        end
    }

    use {
        'neovim/nvim-lspconfig',
    }

    use {
        'L3MON4D3/LuaSnip'
    }

    use {
        'hrsh7th/nvim-cmp',

        requires = {
            {'hrsh7th/cmp-nvim-lsp'},
            {'hrsh7th/cmp-buffer'},
            {'hrsh7th/cmp-path'},
            {'hrsh7th/cmp-cmdline'}, 
            {'saadparwaiz1/cmp_luasnip'}
        },
            
        config = function()

            local cmp = require'cmp'

            cmp.setup {
                snippet = {
                    expand = function(args)
                        require('luasnip').lsp_expand(args.body)
                    end,
                },

                mapping = {
                    ['<C-b>'] = cmp.config.disable,
                    ['<C-f>'] = cmp.config.disable,
                    ['<C-Space>'] = cmp.mapping(cmp.mapping.complete(), { 'i', 'c' }),
                    ['<C-y>'] = cmp.config.disable, -- Specify `cmp.config.disable` if you want to remove the default `<C-y>` mapping.
                    ['<C-e>'] = cmp.mapping {
                        i = cmp.mapping.abort(),
                        c = cmp.mapping.close(),
                    },
                    ['<CR>'] = cmp.mapping.confirm({ select = true }), -- Accept currently selected item. Set `select` to `false` to only confirm explicitly selected items.
                },
                sources = cmp.config.sources {
                    { name = 'nvim_lsp' },
                    { name = 'luasnip' }, -- For luasnip users.
                    { name = 'neorg' }, -- For neorg users.
                    { name = 'buffer' },
                }
            }

            -- Use buffer source for `/` (if you enabled `native_menu`, this won't work anymore).
            cmp.setup.cmdline('/', {
                sources = {
                    { name = 'buffer' },
                }
            })

            -- Use cmdline & path source for ':' (if you enabled `native_menu`, this won't work anymore).
            cmp.setup.cmdline(':', {
                sources = cmp.config.sources {
                    { name = 'path' },
                    { name = 'cmdline' },
                }
            })

            local capabilities = require('cmp_nvim_lsp').update_capabilities(vim.lsp.protocol.make_client_capabilities())
            require('lspconfig').vimls.setup {
                capabilities = capabilities,
            }

            require('lspconfig').pyright.setup {
                capabilities = capabilities,
            }

            require'lspconfig'.tsserver.setup {
                capabilities = capabilities
            }

            --- Loads the Neorg completion module
            local neorg = require('neorg')

            local function load_completion()
                neorg.modules.load_module("core.norg.completion", nil, {
                    engine = "nvim-cmp" -- Choose your completion engine here
                })
            end

            -- If Neorg is loaded already then don't hesitate and load the completion
            if neorg.is_loaded() then
                load_completion()
            else -- Otherwise wait until Neorg gets started and load the completion module then
                neorg.callbacks.on_event("core.started", load_completion)
            end


        end
    }

    use {
        'windwp/nvim-autopairs',
        config = function()
            require('nvim-autopairs').setup{}
        end
    }

    use {
        'nvim-treesitter/nvim-treesitter',
        run = ':TSUpdate',

        config = function()
            local parser_configs = require('nvim-treesitter.parsers').get_parser_configs()

            parser_configs.norg = {
                install_info = {
                    url = "https://github.com/nvim-neorg/tree-sitter-norg",
                    files = { "src/parser.c", "src/scanner.cc" },
                    branch = "main"
                },
            }

            parser_configs.norg_meta = {
                install_info = {
                    url = "https://github.com/nvim-neorg/tree-sitter-norg-meta",
                    files = { "src/parser.c" },
                    branch = "main"
                },
            }

            parser_configs.norg_table = {
                install_info = {
                    url = "https://github.com/nvim-neorg/tree-sitter-norg-table",
                    files = { "src/parser.c" },
                    branch = "main"
                },
            }

            require('nvim-treesitter.configs').setup {
                ensure_installed = {"norg", "norg_meta", "norg_table", "lua", "python", "vim", "markdown", "latex"},
                sync_install = false,

                highlight = {
                    enable = true,
                }
            } 
        end
    }

    use {
        'tpope/vim-commentary', -- old but gold

        config = function()
            vim.api.nvim_set_keymap('n', '<leader>c',  [[:Commentary<cr>]], { noremap = true, silent = true })
            vim.api.nvim_set_keymap('v', '<leader>c',  [[:'<,'>Commentary<cr>]], { noremap = true, silent = true })
        end
    }

    use {
        'tpope/vim-fugitive',

        config = function()
            vim.api.nvim_set_keymap('n', '<leader>gg',  [[:G<cr>]], { noremap = true, silent = true })
        end
    }

    use {
        'tpope/vim-dispatch',

        config = function()
            vim.api.nvim_set_keymap('n', '<leader>d', [[:Dispatch<cr>]], { noremap = true, silent = true })

            vim.cmd[[
            augroup Dispatch
                autocmd FileType python let b:dispatch= 'python %'
                autocmd FileType typescript let b:dispatch= 'ts-run %'
            ]]
        end
    }

    use {
        'nvim-neorg/neorg',
        config = function()

            require('neorg').setup {
                -- Tell Neorg what modules to load
                load = {
                    ["core.defaults"] = {}, -- Load all the default modules
                    ["core.norg.concealer"] = {}, -- Allows for use of icons
                    ["core.norg.dirman"] = { -- Manage your directories with Neorg
                        config = {
                            workspaces = {
                                my_workspace = "~/neorg"
                            }
                        }
                    }
                }
            }
        end,

        requires = "nvim-lua/plenary.nvim"
    }
end)
