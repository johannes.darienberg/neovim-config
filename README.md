# Johannes' init.lua
This is my nvim config

## Prerequisites
You need nvim 0.6+ for telescope and packer
```sh
git clone --depth 1 https://github.com/wbthomason/packer.nvim\
 ~/.local/share/nvim/site/pack/packer/start/packer.nvim
```

## Replicate config
You have to install neovim via yay/AppImage and then
```vim
:PackerCompile
:PackerInstall
:TSInstall
```
