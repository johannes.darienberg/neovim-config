set guicursor=
set termguicolors
set completeopt=menuone,noinsert,noselect
set updatetime=100
set timeoutlen=300
set noshowmode
set list
set listchars=eol:↴


" Local to window
set number relativenumber
set nowrap

" Local to buffer
set tabstop=4
set shiftwidth=4
set noswapfile
set expandtab

let mapleader = " "

call plug#begin()

Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'
Plug 'sheerun/vim-polyglot'

Plug 'akinsho/nvim-bufferline.lua'

Plug 'rcarriga/nvim-notify'
Plug 'mbbill/undotree'

Plug 'neovim/nvim-lspconfig'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/nvim-cmp'
Plug 'hrsh7th/vim-vsnip'

Plug 'tpope/vim-surround'
Plug 'tpope/vim-commentary'
Plug 'cohama/lexima.vim'

Plug 'tpope/vim-fugitive'
Plug 'airblade/vim-gitgutter'

Plug 'vimwiki/vimwiki'

Plug 'rebelot/kanagawa.nvim'

Plug 'lukas-reineke/indent-blankline.nvim'
Plug 'akinsho/nvim-bufferline.lua'
Plug 'nvim-lualine/lualine.nvim'

Plug 'voldikss/vim-floaterm'

call plug#end()


let g:currentmode = {
    \ '__'     : '-',
    \ 'c'      : 'C',
    \ 'i'      : 'I',
    \ 'ic'     : 'I',
    \ 'ix'     : 'I',
    \ 'n'      : 'N',
    \ 'multi'  : 'M',
    \ 'ni'     : 'N',
    \ 'no'     : 'N',
    \ 'R'      : 'R',
    \ 'Rv'     : 'R',
    \ 's'      : 'S',
    \ 'S'      : 'S',
    \ 't'      : 'T',
    \ 'v'      : 'V',
    \ 'V'      : 'V',
    \ '^V'     : 'V',
    \ }

function GitBranch()
  let l:branchname = system("git rev-parse --abbrev-ref HEAD 2>/dev/null | tr -d '\n'")
  return strlen(l:branchname) > 0?''.l:branchname.'':''
endfunction

" set statusline=%{toupper(g:currentmode[mode()])}\ \|\ %F%h\ %{GitBranch()}\ %=\ %l:%c\ \|\ \%m%r%h%w%y

" Lua plugins
lua <<EOF
local cmp = require'cmp'

cmp.setup({
  snippet = {
    -- REQUIRED - you must specify a snippet engine
    expand = function(args)
      vim.fn["vsnip#anonymous"](args.body) -- For `vsnip` users.
    end,
  },
  mapping = {
    ['<C-b>'] = cmp.mapping(cmp.mapping.scroll_docs(-4), { 'i', 'c' }),
    ['<C-f>'] = cmp.mapping(cmp.mapping.scroll_docs(4), { 'i', 'c' }),
    ['<C-Space>'] = cmp.mapping(cmp.mapping.complete(), { 'i', 'c' }),
    ['<C-y>'] = cmp.config.disable, -- Specify `cmp.config.disable` if you want to remove the default `<C-y>` mapping.
    ['<C-e>'] = cmp.mapping({
      i = cmp.mapping.abort(),
      c = cmp.mapping.close(),
    }),
    -- Accept currently selected item. If none selected, `select` first item.
    -- Set `select` to `false` to only confirm explicitly selected items.
    ['<CR>'] = cmp.mapping.confirm({ select = true }),
  },
  sources = cmp.config.sources({
    { name = 'nvim_lsp' },
    { name = 'vsnip' }, -- For vsnip users.
  }, {
    { name = 'buffer' },
  })
})

cmp.setup.cmdline('/', {
  sources = {
    { name = 'buffer' }
  }
})

cmp.setup.cmdline(':', {
  sources = cmp.config.sources({
    { name = 'path' }
  }, {
    { name = 'cmdline' }
  })
})


require('bufferline').setup {
    options = {
        numbers = "none",
        separator_style = "slant",
        sort_by = "directory",
        show_buffer_close_icons = false,
        show_close_icon = false,
        tab_size = 20,
        offsets = {},
    },

    highlights = {
        tab = {
            guibg = {
                attribute = "bg",
                highlight = "TabLine"
            }
        },
        buffer_selected = {
            gui = "none"
        }
    },

}

-- Setup lspconfig.
local capabilities = require('cmp_nvim_lsp').update_capabilities(vim.lsp.protocol.make_client_capabilities())
require'lspconfig'.vimls.setup{
  capabilities = capabilities
}
require'lspconfig'.pyright.setup{
  capabilities = capabilities
}

local actions = require("telescope.actions")
require("telescope").setup{
  defaults = {
    mappings = {
      i = {
        ["<esc>"] = actions.close
      },
    },
  }
}

require('kanagawa').setup({
    undercurl = true,           -- enable undercurls
    commentStyle = "italic",
    functionStyle = "NONE",
    keywordStyle = "italic",
    statementStyle = "italic",
    typeStyle = "italic",
    variablebuiltinStyle = "italic",
    specialReturn = true,       -- special highlight for the return keyword
    specialException = true,    -- special highlight for exception handling keywords
    transparent = false,        -- do not set background color
    colors = {},
    overrides = {},
})

require'lualine'.setup {
  options = {
    icons_enabled = false,
    theme = 'auto',
    component_separators = { left = '', right = ''},
    section_separators = { left = '', right = ''},
    disabled_filetypes = {},
    always_divide_middle = true,
  },
  sections = {
    lualine_a = {'mode'},
    lualine_b = {'branch', 'diff', 'diagnostics'},
    lualine_c = {'filename'},
    lualine_x = {'encoding', 'fileformat', 'filetype'},
    lualine_y = {'progress'},
    lualine_z = {'location'}
  },
  inactive_sections = {
    lualine_a = {},
    lualine_b = {},
    lualine_c = {'filename'},
    lualine_x = {'location'},
    lualine_y = {},
    lualine_z = {}
  },
  tabline = {},
  extensions = {}
}
EOF

colorscheme kanagawa " Colorscheme after setup

imap <expr> <C-x>e vsnip#expandable()  ? '<Plug>(vsnip-expand)'         : '<C-x>e'
smap <expr> <C-x>e vsnip#expandable()  ? '<Plug>(vsnip-expand)'         : '<C-x>e'
imap <expr> <C-x>x vsnip#available(1)  ? '<Plug>(vsnip-expand-or-jump)' : '<C-x>x'
smap <expr> <C-x>x vsnip#available(1)  ? '<Plug>(vsnip-expand-or-jump)' : '<C-x>x'
imap <expr> <C-x>n vsnip#jumpable(1)   ? '<Plug>(vsnip-jump-next)'      : '<C-x>n'
smap <expr> <C-x>n vsnip#jumpable(1)   ? '<Plug>(vsnip-jump-next)'      : '<C-x>n'
imap <expr> <C-x>N vsnip#jumpable(-1)  ? '<Plug>(vsnip-jump-prev)'      : '<C-x>N'
smap <expr> <C-x>N vsnip#jumpable(-1)  ? '<Plug>(vsnip-jump-prev)'      : '<C-x>N'
nmap <expr> <C-x>s <Plug>(vsnip-select-text)
xmap <expr> <C-x>s <Plug>(vsnip-select-text)
nmap <expr> <C-x>S <Plug>(vsnip-cut-text)
xmap <expr> <C-x>S <Plug>(vsnip-cut-text)

nnoremap <silent> <leader>ce :e $MYVIMRC<CR>
nnoremap <silent> <leader>cr :so $MYVIMRC<CR>
nnoremap <silent> <leader>ci :PlugInstall<CR>
nnoremap <silent> <leader>cc :PlugClean<CR>

nnoremap <silent> <leader>gg :Git<CR>
nnoremap <silent> <leader>gp :Git push<CR>

nnoremap <silent> <C-l> :bn<CR>
nnoremap <silent> <C-h> :bp<CR>

nnoremap <silent> <leader>t :term<CR>
nnoremap <silent> <leader>fa :FloatermNew<CR>
nnoremap <silent> <leader>fk :FloatermKill<CR>
nnoremap <silent> <leader>ft :FloatermToggle<CR>
nnoremap <silent> <leader>fn :FloatermNext<CR>
nnoremap <silent> <leader>fp :FloatermPrev<CR>

nnoremap <silent> <leader>c :Commentary<CR>

nnoremap <silent> <C-p> :Telescope find_files<CR>

tnoremap <silent> <f1> <C-\> <C-n>

augroup Binary
  au!
  au BufReadPre  *.bin let &bin=1
  au BufReadPost *.bin if &bin | %!xxd -b
  au BufReadPost *.bin set ft=xxd | endif
  au BufWritePre *.bin if &bin | %!xxd -r
  au BufWritePre *.bin endif
  au BufWritePost *.bin if &bin | %!xxd
  au BufWritePost *.bin set nomod | endif
augroup END

augroup Formatting
    autocmd BufWritePre * :%s/\s\+$//e " Trim spaces
augroup END
